#!/usr/bin/env python3

import glob
import os
import sys


class PackageInfo(object):
    def __init__(self, pkg_name, pkg_destination, pkg_hash, pkg_version, pkg_directory, pkg_dependencies):
        self.name = pkg_name
        self.destination = pkg_destination
        self.hash = pkg_hash
        self.version = pkg_version
        self.directory = pkg_directory
        self.dependencies = pkg_dependencies
        self.is_subpackage = (pkg_name != pkg_destination)
        self.is_generator = "MCGenerators" in pkg_directory


def fake_meta_package(name, lcg_version, platform):
    with open("/cvmfs/sft.cern.ch/lcg/releases/LCG_%s/LCG_externals_%s.txt" % (lcg_version, platform), "r") as old_file:
        for line in old_file.readlines():
            if line.startswith(name):
                pkg_hash = line.split(";")[1].strip()
                pkg_version = line.split(";")[2].strip()
                pkg_directory = line.split(";")[3].strip()
                pkg_dependencies = set((line.split(";")[4].strip()).split(","))

                return PackageInfo(name, name, pkg_hash, pkg_version, pkg_directory, pkg_dependencies)


def get_fields(build_info_content):
    fields = {}
    has_reached_dependencies_field = False
    has_old_format = 'DEPENDS' not in build_info_content

    for field in build_info_content.split(','):
        if not field.strip():
            continue
        if not has_reached_dependencies_field:
            key, value = map(str.strip, (field.split(':')[0], ':'.join(field.split(':')[1:])))
            if key == 'DEPENDS':
                fields[key] = value and [value] or []
                has_reached_dependencies_field = True
            elif has_old_format and key == 'VERSION':
                fields[key] = value
                fields['DEPENDS'] = []
                has_reached_dependencies_field = True
            else:
                fields[key] = value
        else:
            if field.strip():
                fields['DEPENDS'].append(field.strip())
    return fields


def create_package_from_file(build_info_directory, build_info_content, packages):
    keys = get_fields(build_info_content)

    compiler_version = keys['COMPILER'].split()[1]
    name = keys['NAME']
    destination = keys['DESTINATION']
    hash = keys['HASH']
    version = keys['VERSION']

    # Handle duplicate entries (like herwig++/herwigpp), only take the "++" case
    if (name + hash) in packages:
        old_package = packages[name + hash]
        if old_package.version == version:
            if old_package.directory < build_info_directory:
                return

    # Handle the dependencies properly
    dependency_list = keys['DEPENDS']
    dependencies = set()
    for dependency in dependency_list:
        dependency_info = dependency.rsplit("-", 5)
        dependency_info_normalized = dependency_info[0] + "-" + dependency_info[-1]
        dependencies.add(dependency_info_normalized)

    if name == "ROOT":
        dependencies.discard("pythia8")

    # Ignore hepmc3
    if name != "hepmc3":
        if "MCGenerators" in build_info_directory:
            packages[name + hash] = PackageInfo(name, destination, hash, version, build_info_directory, dependencies)
        else:
            packages[name] = PackageInfo(name, destination, hash, version, build_info_directory, dependencies)

    return compiler_version


if __name__ == "__main__":

    if len(sys.argv) != 4:
        print("Please provide DIRECTORY, PLATFORM and LCG_VERSION as command line parameters")
        sys.exit()

    _, target_directory, platform, lcg_version = sys.argv

    compiler = "".join([s for s in platform.split("-")[2] if not s.isdigit()])
    compiler_version = ""

    packages = {}

    # Collect the .buildinfo_<name>.txt files for all packages, generators and grid externals
    files = glob.glob(os.path.join(target_directory, '*/*', platform, '.buildinfo_*.txt'))
    files.extend(glob.glob(os.path.join(target_directory, 'MCGenerators/*/*', platform, '.buildinfo_*.txt')))
    files.extend(glob.glob(os.path.join(target_directory, 'Grid/*/*', platform, '.buildinfo_*.txt')))

    for build_info in files:
        build_directory, _ = os.path.split(build_info)

        with open(build_info, "r") as build_info_file:
            content = build_info_file.read()

        # Find latest compiler version in a mixed release, e.g. gcc7.1.0, gcc7.3.0, gcc7.2.0 -> gcc7.3.0
        tmp_compiler_version = create_package_from_file(build_directory, content, packages)

        if tmp_compiler_version > compiler_version:
            compiler_version = tmp_compiler_version

    # Traverse the entire dependency lists:
    #  - Every dependency of a subpackage is forwarded to the real package
    #  - Sometimes not an entire meta-package is built so we need to create fake packages to replace them
    #  - These are used during dependency resolution, but deleted before the summary is written out
    fake_packages = {}

    for name, package in packages.items():
        if package.is_subpackage:
            # check whether the meta-package was actually fully built
            # if not, insert a temporary fake package
            if package.destination in packages:
                packages[package.destination].dependencies.update(package.dependencies)
            else:
                fake_packages[package.destination] = fake_meta_package(package.destination, lcg_version, platform)
    packages.update(fake_packages)

    # now remove all subpackages in dependencies and replace them by real packages
    for name, package in packages.items():
        if not package.is_subpackage:

            packages_to_remove = set()
            packages_to_add = set()

            for dep in package.dependencies:
                dep_name = dep.split("-")[0]
                if dep_name in packages:
                    if packages[dep_name].is_subpackage:
                        packages_to_remove.add(dep)
                        destination = packages[dep_name].destination
                        packages_to_add.add("%s-%s" % (destination, packages[destination].hash))

            for dep in packages_to_remove:
                package.dependencies.remove(dep)

            for dep in packages_to_add:
                package.dependencies.add(dep)

        # make sure that a meta-package doesn't depend on itself
        package.dependencies.discard(name)

    # now remove the fake packages again...
    for fake in fake_packages.keys():
        del packages[fake]

    # Write the externals and generators to disk
    externals_filename = target_directory + "/LCG_externals_%s.txt" % platform
    generators_filename = target_directory + "/LCG_generators_%s.txt" % platform

    with open(externals_filename, "w") as externals, open(generators_filename, "w") as generators:
        externals.write("PLATFORM: %s\n" % platform)
        externals.write("VERSION: %s" % lcg_version)
        externals.write("COMPILER: %s;%s\n" % (compiler, compiler_version))

        generators.write("PLATFORM: %s\n" % platform)
        generators.write("VERSION: %s" % lcg_version)
        generators.write("COMPILER: %s;%s\n" % (compiler, compiler_version))

        for package in packages.values():
            if not package.is_subpackage:
                dependency_string = ",".join(package.dependencies)
                summary = "%s; %s; %s; %s; %s" % (package.name, package.hash, package.version, package.directory, dependency_string)

                if package.is_generator:
                    generators.write(summary + "\n")
                else:
                    externals.write(summary + "\n")
