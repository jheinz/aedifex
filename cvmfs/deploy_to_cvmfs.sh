#!/usr/bin/env bash

set -e # Fail script on first error
shopt -s nullglob # Don't fail on empty array loops
#shopt -s nocasematch

# English name of today's weekday as 3 letter abbreviation
TODAY="$( date +%a )"

# Check environment variables, exit if they are not set
# Expecting values like LCG_VERSION=dev3, BUILDMODE=nightly, PLATFORM=x86_64-centos7-gcc7-opt, REPOSITORY=sft-nightlies.cern.ch, TODAY=Mon
echo
echo "------------------------------------------------------------------------------------------"
echo "REPOSITORY:  ${REPOSITORY:?You need to set REPOSITORY (non-empty)}"
echo "TODAY:       ${TODAY:?You need to set TODAY (non-empty)}"
echo "LCG_VERSION: ${LCG_VERSION:?You need to set LCG_VERSION (non-empty)}"
echo "PLATFORM:    ${PLATFORM:?You need to set PLATFORM (non-empty)}"
echo "COMPILER :   ${COMPILER:?You need to set COMPILER (non-empty)}"
echo "BUILDMODE:   ${BUILDMODE:?You need to set BUILDMODE (non-empty)}"
echo "------------------------------------------------------------------------------------------"
echo

# Check the given CVMFS repository
if [[ ! "${REPOSITORY}" =~ ^sft(-nightlies)?.cern.ch$ ]]
then
    echo "[ERROR] ${REPOSITORY} is an invalid CVMFS repository."
    exit 1
fi

CURL_CMD="\curl --silent --fail"


# Retries a command a with an exponential backoff strategy.
# Based on https://gist.github.com/fernandoacorreia/b4fa9ae88c67fa6759d271b743e96063
function retry {
    local max_attempts=5
    local timeout=1
    local attempt=1

    set -o pipefail

    while (( ${attempt} <= ${max_attempts} ))
    do
        set +e
        "$@"

        if [[ $? == 0 ]]
        then
            break
        elif (( ${attempt} == ${max_attempts} ))
        then
            echo "[ERROR] Attempt ${attempt}/${max_attempts} failed ($@). Maximum retries exceeded." 1>&2
        else
            echo "[WARNING] Attempt ${attempt}/${max_attempts} failed ($@). Retrying in ${timeout}s..." 1>&2
            sleep ${timeout}
            attempt=$(( attempt + 1 ))
            timeout=$(( timeout * 2 ))
        fi
    done
    set -e
}


# Deploy a file from a given URL to a local target after checking its availability
function deploy_if_exists {
    DESTINATION="$1"
    SOURCE="$2"
    if ${CURL_CMD} --head --output /dev/null "${SOURCE}"
    then
        echo "[INFO] Downloading ${SOURCE}"
        wget --no-verbose --directory-prefix=${DESTINATION} ${SOURCE}
    else
        echo "[WARN] URL does not exist: ${SOURCE}. Proceeding without error."
    fi
}

# Check if the CVMFS repository on this machine is currently in a transaction
function repository_in_transaction {
    cvmfs_server list | grep "${REPOSITORY}" | grep "in transaction" > /dev/null;
}

# Define a fallback function to make sure we don't the transaction open
function cancel_transaction {
    echo "[ERROR] Canceling deploy_to_cvmfs.sh unexpectedly"
    if repository_in_transaction
    then
        echo "[WARN] This script was in a CVMFS transaction. Aborting ..."
        cvmfs_server abort -f ${REPOSITORY}
    else
        echo "[INFO] This script was not in a transaction."
    fi
    exit 1
}

# Register the function cancel_transaction to the ERR signal (called after every failure before exit)
trap cancel_transaction ERR


if repository_in_transaction
then
    # No re-tries since this script should be run inside a cronjob every 30 minutes or so.
    echo "[INFO] ${REPOSITORY} is already in a transaction. Exiting ..."
    exit 0
else
    echo "[INFO] Opening a transaction in ${REPOSITORY} ..."
    cvmfs_server transaction ${REPOSITORY}
fi


#TODO export necessary?
export COMPILER=${COMPILER}
export LCG_VERSION=${LCG_VERSION}


if [[ ${BUILDMODE} == "nightly" ]]
then
    
    EOS_SOURCE="https://lcgpackages.web.cern.ch/lcgpackages/tarFiles/nightlies/${LCG_VERSION}/${TODAY}"
    CVMFS_TARGET="/cvmfs/${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/${TODAY}"
    
    #
    # [STAGE 1] Clean stuff from the nightly build one week ago
    #
    echo "------------------------------------------------------------------------------------------"
    
    # Remove status files such as isDone, isDone-unstable, LCG_contrib.txt, LCG_externals.txt, LCG_generators.txt
    \rm -fv /cvmfs/${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/${TODAY}/*${PLATFORM}*
    
    # Delete all old packages. The wildcard '*/*' refers to package name and version.
    CVMFS_PACKAGES="${CVMFS_TARGET}/*/*/${PLATFORM}"
    
    # Generateors and Grid packages are nested one level deeper ('*/*/*')
    CVMFS_GENERATORS="${CVMFS_TARGET}/*/*/*/${PLATFORM}"
    
    # Delete platform specific package folders or symlinks to releases
    for OLDIE in $( \ls -1d ${CVMFS_PACKAGES} ${CVMFS_GENERATORS} )
    do
        # Check if it is a symlink or a real folder
        if [[ -L ${OLDIE} ]]
        then
            # Delete only the symlink
            \rm -fv ${OLDIE}
        else
            # Delete also the files in the subfolders
            \rm -rfv ${OLDIE}
        fi
    done
    
    # Delete empty package or version folders that are now empty after the previous deletion of platform
    for OLDIE in $( \find ${CVMFS_TARGET} -maxdepth 2 -depth -empty -type d )
    do
        \rmdir -v ${OLDIE}
    done
    
    #
    # [STAGE 2] Install files from EOS
    #
    echo "------------------------------------------------------------------------------------------"
    
    deploy_if_exists "${CVMFS_TARGET}" "${EOS_SOURCE}/isDone-${PLATFORM}"
    deploy_if_exists "${CVMFS_TARGET}" "${EOS_SOURCE}/isDone-unstable-${PLATFORM}"

    # Download, read and delete a list of all tarballs in this nightly release on EOS
    ${CURL_CMD} ${EOS_SOURCE}/tarballs-${PLATFORM}.sh > ${WORKSPACE}/tarballs-${PLATFORM}.sh
    source ${WORKSPACE}/tarballs-${PLATFORM}.sh
    rm -fv ${WORKSPACE}/tarballs-${PLATFORM}.sh

    BUILD_INFO="${EOS_SOURCE}/LCG_${LCG_VERSION}_${PLATFORM}.txt"

    # TODO Check availability of all tarballs on EOS (move from lcginstall script)
    #for TARBALL_URL in ${TARBALLS[@]}
    #do
    #    ${CURL_CMD} --head --output /dev/null "${TARBALL_URL}"
    #done

    for TARBALL in ${TARBALLS[@]}
    do
        DOWNLOAD_CMD="${CURL_CMD} ${EOS_SOURCE}/${TARBALL}"
        EXTRACT_CMD="\tar --verbose --extract --gunzip --preserve-permissions --directory=${CVMFS_TARGET} --file -"

        # Redirect stdout from download directly to the extraction without creating an intermediate file
        ${DOWNLOAD_CMD} | ${EXTRACT_CMD}

        # TODO: Define ${PKG_DIR} and ${PKG_VERSION} for a given TARBALL

        # Change into the directory of the package/version/platform
        pushd ${CVMFS_TARGET}/${PKG_DIR}/${PKG_VERSION}/${PLATFORM} > /dev/null

        # Create an empty .cvmfscatalog file as indicators for CVMFS to create a catalog here
        touch .cvmfscatalog

        # Run .post-install.sh scripts for each new package if it exists
        if [[ -e ./.post-install.sh ]]
        then
            echo "[INFO] Executing .post-install.sh script ..."
            env NIGHTLY_MODE=1 INSTALLDIR=${CVMFS_TARGET} bash -e ./.post-install.sh
        else
            echo "[WARN] .post-install.sh doesn't exist"
        fi

        # Switch back to the cvsft $HOME folder (necessary for publish/abort command)
        popd > /dev/null
    done





    #################################################################################
    # TODO : EOS installation script
    #################################################################################
    # ${WORKSPACE}/lcgjenkins/lcginstall.py 
    #   -y 
    #   -u ${EOS_SOURCE} 
    #   -r ${LCG_VERSION} 
    #   -d LCG_${LCG_VERSION}_${PLATFORM}.txt 
    #   -p ${CVMFS_TARGET}/
    #   -e cvmfs                                  # <-- completely useless
    #################################################################################

    # TODO PATH to script
    python3 deploy_to_cvmfs.py



    #
    # [STAGE 3] Extract build summary and create LCG_contrib.txt, LCG_externals.txt, LCG_generators.txt
    #
    echo "------------------------------------------------------------------------------------------"

    python3 extract_summary.py ${CVMFS_TARGET} ${PLATFORM} ${LCG_VERSION}

    # Copy the contrib file from the releases
    cp /cvmfs/sft.cern.ch/lcg/releases/LCG_contrib_${PLATFORM}.txt ${CVMFS_TARGET}/LCG_contrib_${PLATFORM}.txt



    # TODO: Create view for NIGHTLIES also if installation has failed?

    # Note: In an earlier version of this script we were creating a view although the previous step (installation)
    #       had failed in order to provide e.g. the latest ROOT even thought a small package has failed to deploy.
    #       I decided to fail now in any case (as we do with releases) and thus skip the entire publication altogether.

    #
    # [STAGE 4] Create a view based on the new files on CVMFS
    #
    echo "------------------------------------------------------------------------------------------"

    if [[ ${VIEWS_CREATION} == "true" ]]
    then
        if [[ -f "${CVMFS_TARGET}/isDone-${PLATFORM}" ]]
        then
            echo "[INFO] Creating views ..."
            CVMFS_VIEW="/cvmfs/${REPOSITORY}/lcg/views/${LCG_VERSION}"
            
            #
            # TODO : View creation script
            #################################################################################
            # ${WORKSPACE}/lcgcmake/cmake/scripts/create_lcg_view.py 
            #   -l ${CVMFS_TARGET} 
            #   -p ${PLATFORM} 
            #   -d 
            #   -B ${CVMFS_VIEW/${TODAY}/${PLATFORM}
            
            # Recreate the symbolic link to the latest view for this LCG version and platform
            rm -fv ${CVMFS_VIEW}/latest/${PLATFORM}
            ln -s ${CVMFS_VIEW}/${TODAY}/${PLATFORM} ${CVMFS_VIEW}/latest/${PLATFORM}
            
        elif [[ -f "${CVMFS_TARGET}/isDone-unstable-${PLATFORM}" ]]
        then
            echo "[WARN] The build was incomplete (isDone-unstable file exists). We'll have to skip the view creation, I'm afraid ..."
        else
            echo "[WARN] There's neither a isDone nor a isDone-unstable file. Skipping view creation ..."
        fi
    else
        echo "[INFO] You chose not to create views. Skipping view creation ..."
    fi
    
    # TODO Mark trigger as done

elif [[ ${BUILDMODE} == "release" ]]
then
    # $WORKSPACE/lcgjenkins/lcginstall.py 
    # -u http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/releases 
    # -r ${LCG_VERSION} 
    # -d LCG_${LCG_VERSION}_${PLATFORM}.txt 
    # -p /cvmfs/sft.cern.ch/lcg/releases 
    # -e cvmfs
    # 
    # cd  /cvmfs/sft.cern.ch/lcg/releases/LCG_${LCG_VERSION}
    # $WORKSPACE/lcgjenkins/extract_LCG_summary.py . ${PLATFORM} ${LCG_VERSION} RELEASE
    # 
    # if [[ ${VIEWS_CREATION} == "true" ]]
    # then
    #     test "\${abort}" == "1" && $WORKSPACE/lcgcmake/cmake/scripts/create_lcg_view.py 
    #                                 -l /cvmfs/sft.cern.ch/lcg/releases 
    #                                 -p ${PLATFORM} 
    #                                 -r ${LCG_VERSION} 
    #                                 -d 
    #                                 -B /cvmfs/sft.cern.ch/lcg/views/LCG_${LCG_VERSION}/${PLATFORM}
    # fi
    echo "[ERROR] Build type 'release' is not yet supported"
    exit 1

elif [[ ${BUILDMODE} == "limited" ]]
then
    # if [[ "${UPDATELINKS}" == "false" ]]
    # then
    #     $WORKSPACE/lcgjenkins/lcginstall.py 
    #     -o 
    #     -u http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/releases 
    #     -r ${LCG_VERSION} 
    #     -d LCG_${LCG_VERSION}_${PLATFORM}.txt 
    #     -p /cvmfs/sft.cern.ch/lcg/releases 
    #     -e cvmfs
    # else
    #     $WORKSPACE/lcgjenkins/lcginstall.py 
    #     --update
    #     -o 
    #     -u http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/releases 
    #     -r ${LCG_VERSION} 
    #     -d LCG_${LCG_VERSION}_${PLATFORM}.txt 
    #     -p /cvmfs/sft.cern.ch/lcg/releases 
    #     -e cvmfs
    # fi
    # 
    # $WORKSPACE/lcgcmake/cmake/scripts/create_lcg_view.py 
    # -l /cvmfs/sft.cern.ch/lcg/releases/LCG_${LCG_VERSION} 
    # -p ${PLATFORM} 
    # -d 
    # -B /cvmfs/sft.cern.ch/lcg/views/LCG_${LCG_VERSION}/${PLATFORM}
    
    echo "[ERROR] Build type 'limited' is not yet supported"
    exit 1
fi

echo "[INFO] Publishing changes to ${REPOSITORY} ..."
cvmfs_server publish ${REPOSITORY}
