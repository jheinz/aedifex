#!/usr/bin/env python

import sys
import json


def parsefile(filename):
    with open(filename, 'r') as file:
        line = [x.strip() for x in file.readlines()[1:]]
        return [parse(x) for x in line]


def parse(text):
    build_info = {}
    text = text.split(', ')

    for key, value in [x.split(':') for x in text]:
        build_info.update({key.strip(): value.strip()})

    if 'VERSION' in build_info:
        if 'DEPENDS' in build_info:
            build_info['DEPENDS'] = [x for x in build_info['DEPENDS'].split(',') if len(x) != 0]
        else:
            tmp = build_info['VERSION'].split(',')
            build_info['VERSION'] = tmp[0]
            build_info['DEPENDS'] = [x for x in tmp[1:] if len(x) != 0]

    if 'GITHASH' in build_info:
        build_info['GITHASH'] = build_info['GITHASH'].replace('"', '').replace("'", '')

    return build_info


if __name__ == "__main__":

    if len(sys.argv) > 1 and '-h' in sys.argv or '--help' in sys.argv:
        print("Usage: {0} [filename |-]".format(sys.argv[0]))
        sys.exit(0)

    if len(sys.argv) == 1 or (len(sys.argv) > 1 and sys.argv[1] == "-"):
        data = sys.stdin.read().strip()
    else:
        try:
            with open(sys.argv[1], 'r') as f:
                data = f.read().strip()
        except Exception as e:
            print("ERROR:", str(e))
            sys.exit(1)

    print(json.dumps(parse(data)))
