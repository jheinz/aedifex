#!/usr/bin/env python3

import sys
import os
import glob


def parse(text):
    build_info = {}
    text = text.split(", ")

    for key, value in [x.split(":") for x in text]:
        build_info.update({key.strip(): value.strip()})

    if "VERSION" in build_info:
        if "DEPENDS" in build_info:
            build_info["DEPENDS"] = [dep for dep in build_info["DEPENDS"].split(",") if len(dep) != 0]
        else:
            tmp = build_info["VERSION"].split(',')
            build_info["VERSION"] = tmp[0]
            build_info["DEPENDS"] = [dep for dep in tmp[1:] if len(dep) != 0]

    if "GITHASH" in build_info:
        build_info["GITHASH"] = build_info["GITHASH"].replace('"', "").replace("'", "")

    return build_info


if __name__ == "__main__":

    if len(sys.argv) < 3:
        print("[ERROR] Please provide the LCG version and the platform as parameter")
        sys.exit(1)

    lcg_version = sys.argv[1]
    platform = sys.argv[2]
    build_info_filename = "LCG_%s_%s.txt" % (lcg_version, platform)

    os.chdir("build/tarfiles")
    tarballs = glob.glob("*.tgz")

    cvmfs_packages = []
    nightly_packages = []

    os.chdir("..")
    with open(build_info_filename, "r") as build_info_file:
        for line in build_info_file:
            if line[0] == "#":
                continue
            data = parse(line)

            pkg_name = data["NAME"]
            pkg_directory = data["DIRECTORY"]
            pkg_version = data["VERSION"]
            pkg_hash = data["HASH"]

            if "PLATFORM" in data:
                pkg_platform = data["PLATFORM"]
            else:
                pkg_platform = platform

            tarball = "%s-%s_%s-%s.tgz" % (pkg_name, pkg_version, pkg_hash, pkg_platform)

            if tarball in tarballs:
                # nightly_packages.append(os.path.abspath("tarfiles/%s" % tarball))
                nightly_packages.append(tarball)
            else:
                cvmfs_packages.append("/cvmfs/sft.cern.ch/lcg/releases/%s/%s-%s/%s" % (
                    pkg_directory, pkg_version, pkg_hash, pkg_platform))

    nightly_packages.sort()
    cvmfs_packages.sort()

    # Save tarballs from nightly build
    with open(build_info_filename[:-4] + "_tarballs.txt", "w") as tarballs_file:
        tarballs_file.writelines("%s\n" % item for item in nightly_packages)

    # Save packages from CVMFS symbolic links
    with open(build_info_filename[:-4] + "_symlinks.txt", "w") as symlinks_file:
        symlinks_file.writelines("%s\n" % item for item in cvmfs_packages)
