#!/usr/bin/env bash

set +x # Disable debug print of each command
set -e # Enable failure of this script if any error occurs during a called command

# TODO: Update ENV list and move to README.md
# Script to run full LCG builds inside docker containers
# Beware the code here highly relies on multiple variable defined inside the Jenkins job configuration:
# $BUILD_MODE         -> Needed inside cmake/ctest commands
# $BUILD_TYPE         -> Needed inside cmake/ctest commands
# $COMPILER           -> Needed inside cmake/ctest commands
# $GIT_COMMIT         -> Needed to avoid repeating update command inside container (otherwise update status appears as failed in cdash)
# $LCG_EXTRA_OPTIONS  -> Needed inside cmake/ctest commands
# $LCG_IGNORE         -> Needed inside cmake/ctest commands
# $LCG_INSTALL_PREFIX -> Needed inside cmake/ctest commands
# $LCG_VERSION        -> Needed inside cmake/ctest commands
# $MODE               -> Mode in CDash, required by CTest
# $RUN_TEST           -> Boolean value if the test should run after build
# $SHELL              -> Needed in some package install instructions using $ENV{SHELL}
# $TARGET             -> Needed inside cmake/ctest commands
# $TEST_LABELS        -> Needed by tests, it determines kind of tests to run
# $USER               -> Needed by some CORAL tests which rely on this env variable


#---{ Choose the correct Docker image for the build environment }----------------------------------
if [[ ${LABEL} == docker_* ]]
then
    LABEL=${LABEL:7}
fi

case ${LABEL} in
    slc6 | centos7 | fedora[2-9][0-9] | ubuntu[1-9][0-9] | ubuntu[1-9][0-9]04 | ubuntu[1-9][0-9]10 )
        BUILD_IMAGE="gitlab-registry.cern.ch/sft/docker/${LABEL}:latest"
        ;;
    *)
        echo "[ERROR] Docker image '${LABEL}' is not configured"
        exit 1
        ;;
esac

UTILITY_IMAGE="gitlab-registry.cern.ch/sft/docker/utility:latest"


#---{ Pull image from the GitLab registry, update if necessary-------------------------------------
docker pull ${BUILD_IMAGE}
docker pull ${UTILITY_IMAGE}


#---{ Prepare number of CPUs to use in the container }---------------------------------------------
TOTAL_CPUS=$( nproc --all )
if [[ -z ${DOCKER_CPUS} ]] || [[ ${DOCKER_CPUS} -gt ${TOTAL_CPUS} ]]|| [[ ${DOCKER_CPUS} -lt 1 ]]
then
    DOCKER_CPUS=${TOTAL_CPUS}
fi


#---{ Prepare folder structure for build }---------------------------------------------------------
echo "[INFO] Cleaning docker folder from previous build ..."
rm -rf ${WORKSPACE}/docker
mkdir -p ${WORKSPACE}/docker


#---{ Set all parameters for the "docker run" command }--------------------------------------------
CONTAINER_WORKSPACE="/workspace"
CONTAINER_NAME="jenkins-${LCG_VERSION}-${LABEL}-${COMPILER}-${BUILD_TYPE}-${BUILD_ID}"

DOCKER_PARAMETERS=$(cat << EOF
    --cpus=${DOCKER_CPUS}
    --env ADDITIONAL_CMAKE_OPTIONS=${ADDITIONAL_CMAKE_OPTIONS}
    --env BUILD_MODE=${BUILD_MODE}
    --env BUILD_TYPE=${BUILD_TYPE}
    --env COMPILER=${COMPILER}
    --env COPY_LOGS=${COPY_LOGS}
    --env INSTALL_SOURCES=${INSTALL_SOURCES}
    --env INSTALL_TARBALLS=${INSTALL_TARBALLS}
    --env LABEL=${LABEL}
    --env LCG_INSTALL_PREFIX=${LCG_INSTALL_PREFIX}
    --env LCG_VERSION=${LCG_VERSION}
    --env RUN_TEST=${RUN_TEST}
    --env SHELL=${SHELL}
    --env TARGET=${TARGET}
    --env SUPPRESS_DEV_WARNINGS=${SUPPRESS_DEV_WARNINGS}
    --env TEST_LABELS=${TEST_LABELS}
    --env USER=sftnight
    --env WORKSPACE=${CONTAINER_WORKSPACE}
    --hostname ${HOSTNAME}-docker
    --mount type=bind,source=/ccache,target=/ccache
    --mount type=bind,source=/cvmfs,target=/cvmfs,readonly
    --mount type=bind,source=${WORKSPACE}/docker,target=${CONTAINER_WORKSPACE}
    --mount type=bind,source=${WORKSPACE}/aedifex,target=/aedifex
    --mount type=bind,source=${WORKSPACE}/lcgcmake,target=/lcgcmake
    --mount type=bind,source=${WORKSPACE}/lcgtest,target=/lcgtest
    --security-opt seccomp=unconfined
    --user sftnight
    --workdir ${CONTAINER_WORKSPACE}
EOF
)


#---{ Prepare a debugging script to interactively run the Docker container }-----------------------
#---{ It runs under the same conditions as the actual build (next step below) }--------------------
cat > ${WORKSPACE}/debug_build.sh << EOF
#!/usr/bin/env bash
docker run -it --rm ${DOCKER_PARAMETERS} --env LCG_IGNORE="${LCG_IGNORE}" --name ${CONTAINER_NAME}-debug \
    --mount type=bind,source=${WORKSPACE}/environment.sh,target=/etc/profile.d/setup_lcg_environment.sh,readonly \
    ${BUILD_IMAGE} bash
EOF
chmod u+x ${WORKSPACE}/debug_build.sh


#---{ Run the LCG CMake build inside the specified Docker container }------------------------------
set +e
set -x
docker run ${DOCKER_PARAMETERS} --env LCG_IGNORE="${LCG_IGNORE}" --name ${CONTAINER_NAME}-build ${BUILD_IMAGE} bash -c "source /aedifex/build/build.sh"
BUILD_RESULT=$?
set -e


#---{ Preserve build properties and environment setup script for the next Jenkins job }------------
if [[ -s ${WORKSPACE}/docker/properties.txt ]]
then
    cp ${WORKSPACE}/docker/properties.txt ${WORKSPACE}
    cp ${WORKSPACE}/docker/environment.sh ${WORKSPACE}
    cp ${WORKSPACE}/docker/controlfile ${WORKSPACE}
else
    echo "[ERROR] The build in docker didn't terminate regularly because the properties.txt file is empty or doesn't exist"
    exit 1
fi



#---{ Former 'experimental' mode: Create view and run tests locally without deployment }-----------
if [[ -n ${RUN_TEST} ]] && ${RUN_TEST} && ( ( [[ -n ${CVMFS_INSTALL} ]] && ! ${CVMFS_INSTALL} ) || [[ -z ${CVMFS_INSTALL} ]] )
then
    echo "[INFO] Former experimental mode: Createing view and running test only locally - without deployment to CVMFS."

    # TODO: Create view and run both tests (Rafal's + Ivan's)
    #---{ Create view and run tests }--------------------------------------------------------------
    #
    # cd ${WORKSPACE}
    # /lcgjenkins/lcgsoft/ReleaseSummaryReader ${SLOT:=${LCG_VERSION}} ${PLATFORM} nightlies
    # /lcgjenkins/lcgsoft/fill_release.py \
    #     -f "${WORKSPACE}/${SLOT}-${PLATFORM}.txt" \
    #     -d "$( date +%Y-%m-%d )" \
    #     -e "Daily publication of the ${SLOT} nightliy build"
    #     -o "NO"
    # /lcgcmake/cmake/scripts/create_lcg_view.py -l ${WORKSPACE}/install -p ${PLATFORM} -d -B ${WORKSPACE}/views
    #
    # export BUILDHOSTNAME=$( hostname )
    #
    # if [[ "${RUN_TEST}" ]]
    # then
    #     cd ${WORKSPACE}
    #     cp ${WORKSPACE}/build/Testing/TAG /lcgtest
    #     cp ${WORKSPACE}/lcgjenkins/macros.cmake ${WORKSPACE}/lcgtest
    #     if [[ ! -e "${WORKSPACE}/lcgtest/runtest-docker.sh" ]]
    #     then
    #         cp -r /lcgtest ${WORKSPACE}/
    #     fi
    #     bash -x ${WORKSPACE}/lcgtest/runtest-docker.sh "${PATTERN:=".*"}" "${VIEW}"
    # fi
fi


#---{ Fix setup: EOS_INSTALL is mandatory for CVMFS_INSTALL }--------------------------------------
if [[ -n ${CVMFS_INSTALL} ]] && ${CVMFS_INSTALL} && ( ( [[ -n ${EOS_INSTALL} ]] && ! ${EOS_INSTALL} ) || [[ -z ${EOS_INSTALL} ]] )
then
    echo "[WARNING] Misconfiguration: Cannot deploy to CVMFS without copying to EOS. Setting EOS_INSTALL to true"
    EOS_INSTALL=true
fi


#---{ Copy build results and artifacts to EOS }----------------------------------------------------
if [[ -n ${EOS_INSTALL} ]] && ${EOS_INSTALL}
then
    docker run ${DOCKER_PARAMETERS} --env LCG_IGNORE="${LCG_IGNORE}" \
        --mount type=bind,source=/ec/conf,target=/credentials,readonly \
        --name ${CONTAINER_NAME}-eos \
        ${UTILITY_IMAGE} bash -c "source /aedifex/eos/deploy.sh"
fi


exit ${BUILD_RESULT}
