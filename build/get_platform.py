#!/usr/bin/env python3

# This script detects the platform based on the current build environment and optional environment
# variables. The default output is a string like 'x86_64+avx2+fma-centos7-gcc7-opt' for example.
# If you pass the parameter 'bash' the script outputs a bunch of bash environment variables instead.
#
# DEPENDENCIES
# ------------
# - Python 3.x
# - distro
#
# USAGE IN BASH
# -------------
# PLATFORM=$( python3 get_platform.py )
#   - Save the returned platform string in a local variable
# export $( python3 get_platform.py bash )
#   - Export the returned list of variables
#
# OPTIONAL INPUT ENVIRONMENT VARIABLES TO INFLUENCE THE PLATFORM
# --------------------------------------------------------------
# CC
#   - description: The compiler command as used in bash
#   - values: 'gcc', 'clang', ...
#   - default: 'gcc'
# COMPILER
#   - description: Overwrite the entire compiler version
#   - values: 'gcc62', 'gcc8', 'clang8', ...
#   - default: Read the compiler version from the compiler command output
# INSTRUCTION_SET
#   - description: Define the SIMD instruction set to use
#   - values: 'sse3', 'avx2+fma', ...
#   - default: ''
# BUILD_TYPE
#   - description: Optimized or Debug build
#   - values: 'dbg', 'opt'
#   - default: 'opt'
#
# EXPORTS THE FOLLOWING ENVIRONMENT VARIABLES IF PARAMETER 'bash' IS SET
# ----------------------------------------------------------------------
# - LCG_HOST_ARCH: The system architecture, e.g. 'x86_64'
# - LCG_HOST_OS: The operating system name, e.g. 'ubuntu'
# - LCG_HOST_OSVERS: The operating system version, e.g. '1804'
# - LCG_HOST_COMP: The compiler name, e.g. 'gcc'
# - LCG_HOST_COMPVERS: The compiler version, e.g. '8'
#
# ALTERNATIVES
# ------------
# - hsf_get_platform.py in https://github.com/HSF/tools
# - getPlatform.py in https://gitlab.cern.ch/sft/lcgjenkins
#
# CHANGELOG
# ---------
# 2019-08-13 / jheinz / Split operating system and compiler in tuples of name and version and export
#                       all platform information as ENVs to influence CMake configuration
# 2019-07-25 / jheinz / Initial rewrite from lcgjenkins getPlattform.py to Python3, dropping Windows,
#                       the Intel compiler icc, some old compiler versions and the ENV 'CXX_OPTIONS'


import os
import platform
import re
import subprocess
import sys

import distro


def read_compiler_version(compiler_command, version_pattern):
    """
    Executes given command as sub process and parses the first line of the result with the given regular expression.
    """
    cmd = subprocess.Popen(compiler_command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

    for line in cmd.stdout:
        first_line = line.decode()
        break

    return re.match(version_pattern, first_line)


def get_architecture():
    """
    Determine system architecture and optional SIMD instruction set
    """
    architecture = platform.machine()
    instruction_set = os.getenv('INSTRUCTION_SET')

    if instruction_set:
        architecture = "%s+%s" % (architecture, instruction_set)

    return architecture


def get_operating_system():
    """
    Determine the Operating System and its version
    """
    operating_system_family = platform.system().lower()

    if operating_system_family == 'darwin':
        operating_system_name = 'mac'
        operating_system_version = ''.join(platform.mac_ver()[0].split('.')[:2])
    elif operating_system_family == 'linux':
        distriution = distro.id().lower()
        distriution_major = distro.major_version(best=True)
        distriution_minor = distro.minor_version(best=True)

        if 'slc' in distriution:
            operating_system_name = 'slc'
            operating_system_version = distriution_major
        elif 'centos' in distriution:
            operating_system_name = 'centos'
            operating_system_version = distriution_major
        elif 'ubuntu' in distriution:
            operating_system_name = 'ubuntu'
            operating_system_version = distriution_major + distriution_minor
        elif 'fedora' in distriution:
            operating_system_name = 'fedora'
            operating_system_version = distriution_major
        elif 'red' in distriution:
            operating_system_name = 'redhat'
            operating_system_version = distriution_major
        else:
            operating_system_name = 'linux'
            operating_system_version = distriution_major + distriution_minor
    else:
        operating_system_name = operating_system_family
        operating_system_version = 'unknown'

    return operating_system_name, operating_system_version


def get_compiler(operating_system_name):
    """
    Determine the compiler version
    """

    env_compiler = os.getenv('COMPILER')
    env_cc = os.getenv('CC')

    if env_compiler and not 'native' in env_compiler.lower() and not env_cc:
        compiler_pattern_match = re.match(r'([a-zA-Z]+)([0-9]+)', env_compiler)
        if compiler_pattern_match:
            compiler_name = compiler_pattern_match.group(1)
            compiler_version = compiler_pattern_match.group(2)
        else:
            compiler_name = env_compiler
            compiler_version = 'unknown'
    else:
        if env_cc:
            compiler_command = env_cc.lower()
        elif operating_system_name == 'mac':
            compiler_command = 'clang'
        else:
            compiler_command = 'gcc'

        if 'gcc' in compiler_command:
            compiler_name = 'gcc'
            matches = read_compiler_version(compiler_command + ' -dumpversion', '([0-9]+)(\\.([0-9]+))?')
            if int(matches.group(1)) >= 7:
                compiler_version = matches.group(1)
            else:
                compiler_version = matches.group(1) + matches.group(3)
        elif 'clang' in compiler_command:
            compiler_name = 'clang'
            matches = read_compiler_version(compiler_command + ' -v', '.*version ([0-9]+)[.]([0-9]+)')
            if operating_system_name != 'mac' and int(matches.group(1)) >= 8:
                compiler_version = matches.group(1)
            else:
                compiler_version = matches.group(1) + matches.group(2)
        else:
            compiler_name = compiler_command
            compiler_version = 'unknown'

    return compiler_name, compiler_version


def get_build_type():
    """
    Determine the build type
    """
    env_build_type = os.getenv('BUILD_TYPE', 'opt')
    return env_build_type if env_build_type in ('dbg', 'opt') else 'unk'


# Combine all four properties of the platform
architecture = get_architecture()
(operating_system_name, operating_system_version) = get_operating_system()
(compiler_name, compiler_version) = get_compiler(operating_system_name)
build_type = get_build_type()

platform = '%s-%s%s-%s%s-%s' % (architecture,
                                operating_system_name, operating_system_version,
                                compiler_name, compiler_version,
                                build_type)

if len(sys.argv) == 2 and 'bash' in sys.argv[1].lower():
    # Export environment variables as input for the CMake configuration
    env_platform = 'PLATFORM=%s' % platform
    env_architecture = 'LCG_HOST_ARCH=%s' % architecture
    env_os_name = 'LCG_HOST_OS=%s' % operating_system_name
    env_os_version = 'LCG_HOST_OSVERS=%s' % operating_system_version
    env_compiler_name = 'LCG_HOST_COMP=%s' % compiler_name
    env_compiler_version = 'LCG_HOST_COMPVERS=%s' % compiler_version

    environment_variables = (env_platform,
                             env_architecture,
                             env_os_name, env_os_version,
                             env_compiler_name, env_compiler_version)
    print(' '.join(environment_variables))
else:
    print(platform)
