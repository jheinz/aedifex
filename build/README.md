# Build scripts

This directory contains all scripts for building LCG CMake inside a Docker container. They are called in the following order:

- `launch_container.sh` updates the local container image and sets all environment variables from Jenkins for the running instance
  - Creates a file called `debug_build.sh` in the workspace with the identical configuration as the actual build script for later debugging
  - `build.sh` Runs the actual buils inside the Docker environemnt
    - `setup_environment.sh` Sets a couple of environment variables
      - `get_platform.py` Sets the `PLATFROM` environment variable
    - `lcgcmake.cmake` Starts the CMake process of LCG CMake by calling `ctest` and deploys all build results to [CDash](http://cdash.cern.ch/index.php?project=LCGSoft)
    - Creates two files during runtime for subsequent builds
      - `setup.sh` to set the git repositories to the same hash as during build time and execute `setup_environment.sh` again. This is used in `debug_build.sh`.
      - `properties.txt` to export a couple of environment variables. This is picked up by the deploy jobs for EOS and CVMFS.
  - `../eos/deploy.sh` deploys the build artifacts to EOS if the ENV `EOS_INSTALL` is set to `true` in Jenkins

## Build time

During the build time, set all the necessary environment variables in Jenkins and start the build process by calling `launch_container.sh` as point of entry.

## Debugging

`cd` into the workspace of the build you want to debug and start `debug_build.sh`. This launches an interactive `bash` session in the Docker container.
